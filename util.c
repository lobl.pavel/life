#include "util.h"
#include <stdarg.h>


void*
srealloc(void *ptr, size_t size) {

  void *new = realloc(ptr, size);
  if (!new) {
    perror("realloc");
    exit(EXIT_FAILURE);
  }

  return new;
}

void*
smalloc(size_t size) {

  void *ptr = malloc(size);
  if (!ptr) {
    perror("malloc");
    exit(EXIT_FAILURE);
  }

  return ptr;
}

#ifdef _WIN

static char* alloc(char* buf, size_t size);

int asprintf(char **strp, const char *fmt, ...){
    
  va_list ap;
  int ret;
    
  va_start(ap, fmt);
  ret = vasprintf(strp, fmt, ap);
  va_end(ap);
  return ret;
}

int vasprintf(char **strp, const char *fmt, va_list ap){

  size_t size = 32;
  char* buf = 0;
  int n = 0;
                     
  for (;;) {
    size *= 2;
    buf = alloc(buf, size);
    if ( !buf ){
      return -1;
    }

    n = vsnprintf(buf, size, fmt, ap);

    // @todo @bug the return value is different for msvc compilers and real
    //            compilers so this conditional must be adjusted.
    if ( n < 0 ) {
      continue;
    }

    break;
  }
                          
  *strp = buf;
  return n;
}

static char* alloc(char* buf, size_t size){
  return (char*)realloc(buf, size);
}

#endif

