
#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>
#include <stdio.h>

void* srealloc(void*, size_t);
void* smalloc(size_t);

#endif

